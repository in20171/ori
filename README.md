## Open In Colab Badge

[![Open In Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/drive/1FxcXzSddZnWzirAwHcqEOwUBihH4H8D-)

Remember to replace the notebook URL in this template with the notebook you want to link to.